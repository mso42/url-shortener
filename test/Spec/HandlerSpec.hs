{-# LANGUAGE OverloadedStrings #-}

module Spec.HandlerSpec
  ( spec,
  )
where

import Control.Monad (forM_)
import Data.Maybe (fromJust)
import qualified Data.Text as Text
import Data.Text.Encoding (encodeUtf8)
import Lib.Alias (computeAlias, fromText, getAliasText)
import Lib.AppContext (AppContext (..), runWithAppContext)
import Lib.Handler
import Lib.Url (fromUtf8, render)
import Lib.UrlStore (UrlStore, emptyInMemoryUrlStore, get, put)
import Spec.Arbitraries ()
import Test.Hspec (Spec, describe, it, shouldBe)
import Test.QuickCheck (property)

data DummyStore = DummyStore

instance UrlStore DummyStore where
  get _ _ = return Nothing
  put _ _ _ = return ()

dummyAppContext :: AppContext DummyStore
dummyAppContext = AppContext DummyStore ""

spec :: Spec
spec = do
  describe "Main handler" $ do
    describe "Shorten request" $ do
      it "rejects invalid URLs" $ do
        let badUrls = ["", "http://", "example.com", "/hello/world", "ftp://example.com", "\0", "\xFF"]
        forM_ badUrls $ \badUrl -> do
          let request = Shorten badUrl
          response <- runWithAppContext dummyAppContext $ mainHandler request
          response `shouldBe` Left (ValidationError "Not a valid URI")

      it "stores the URL under an alias" $
        property $ \validUri serverPrefix -> do
          let bsUrl = encodeUtf8 $ render validUri
          store <- emptyInMemoryUrlStore
          let context = AppContext store serverPrefix
          let request = Shorten bsUrl
          Right (Created returnedAliasText) <- runWithAppContext context $ mainHandler request
          let (prefix, aliasText') = Text.breakOnEnd "/" returnedAliasText
              alias = fromJust $ fromText aliasText'
          Just storedUrl <- get store alias
          alias `shouldBe` computeAlias validUri
          storedUrl `shouldBe` validUri
          Text.init prefix `shouldBe` serverPrefix

    describe "Follow request" $ do
      it "rejects invalid aliases" $ do
        let badAliases = ["", "........", "hello/world"]
        forM_ badAliases $ \badAlias -> do
          let request = Follow badAlias
          response <- runWithAppContext dummyAppContext $ mainHandler request
          response `shouldBe` Left (ValidationError "Not a valid alias")

      it "returns a previously-stored URL" $
        property $ \validUri alias serverPrefix -> do
          store <- emptyInMemoryUrlStore
          () <- put store alias validUri
          let context = AppContext store serverPrefix
          let request = Follow $ getAliasText alias
          Right (Redirect redirectUri) <- runWithAppContext context $ mainHandler request
          let parsedRedirectUrl = fromJust $ fromUtf8 $ encodeUtf8 redirectUri
          parsedRedirectUrl `shouldBe` validUri

      it "returns not found for an unknown but valid alias" $
        property $ \alias -> do
          let request = Follow $ getAliasText alias
          response <- runWithAppContext dummyAppContext $ mainHandler request
          response `shouldBe` Left NotFound
