{-# LANGUAGE OverloadedStrings #-}

module Spec.AliasSpec
  ( spec,
  )
where

import Control.Monad (forM_)
import qualified Data.ByteString as BS
import qualified Data.Text as T
import Data.Text.Encoding (encodeUtf8)
import Data.Word (Word8)
import Lib.Alias
import Spec.Arbitraries ()
import Test.Hspec (Spec, describe, it, shouldBe, shouldNotBe, shouldSatisfy)
import Test.QuickCheck (property, (==>))

webSafeCharacters :: [Word8]
webSafeCharacters = BS.unpack "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789-_="

spec :: Spec
spec = do
  describe "Alias" $ do
    describe "computeAlias" $ do
      -- 8 characters of base64 = 48 bits of entropy => 1 in 1000 probability
      -- of a hash collision at 1,000,000 items

      it "returns aliases of length 8 characters" $
        property $ \validUri -> do
          let computed = computeAlias validUri
              text = getAliasText computed
          text `shouldSatisfy` (== 8) . T.length

      it "produces valid web-safe strings" $
        property $ \validUri -> do
          let computed = computeAlias validUri
              text = getAliasText computed
              bs = encodeUtf8 text
          bs `shouldSatisfy` BS.all (`elem` webSafeCharacters)

      it "produces unequal values for unequal inputs" $
        property $ \uri1 uri2 ->
          (uri1 /= uri2) ==> do
            let alias1 = computeAlias uri1
                alias2 = computeAlias uri2
            alias1 `shouldNotBe` alias2

    describe "fromText" $ do
      it "returns Nothing if the alias is not 8 characters" $ do
        let badAliases = ["", "aaaaaaaaa"]
        forM_ badAliases $ \badAlias ->
          fromText badAlias `shouldBe` Nothing

      it "returns Nothing if the alias contained non-web-safe characters" $
        property $ do
          let badAliases = ["aaaaaaaa\xFF", "aaaa/aaa", "aaaa?aaa"]
          forM_ badAliases $ \badAlias ->
            fromText badAlias `shouldBe` Nothing

      it "returns Just for a round trip" $
        property $ \alias -> do
          (fromText . getAliasText) alias `shouldBe` Just alias
