{-# LANGUAGE OverloadedStrings #-}

module Spec.UrlStoreSpec
  ( spec,
  )
where

import Lib.UrlStore
import Spec.Arbitraries ()
import Test.Hspec (Spec, describe, it, shouldBe)
import Test.QuickCheck (property)

spec :: Spec
spec = do
  describe "In-memory URL store" $ do
    it "initialises an empty store" $
      property $ \alias -> do
        store <- emptyInMemoryUrlStore
        url <- get store alias
        url `shouldBe` Nothing

    it "gets something that was put" $
      property $ \alias url -> do
        store <- emptyInMemoryUrlStore
        () <- put store alias url
        returned <- get store alias
        returned `shouldBe` Just url
