{-# LANGUAGE OverloadedStrings #-}

module Spec.Arbitraries where

import Data.List.NonEmpty (NonEmpty ((:|)))
import Data.Maybe (fromJust)
import qualified Data.Text as Text
import qualified Data.Text.Lazy as LText
import Lib.Alias (Alias, computeAlias)
import Lib.Url (ValidURI, validateURI)
import Test.QuickCheck (Arbitrary, arbitrary, genericShrink, oneof, shrink, suchThat)
import Text.URI (URI (URI), authHost, mkHost, mkScheme)

instance Arbitrary Text.Text where
  arbitrary = Text.pack <$> arbitrary

instance Arbitrary LText.Text where
  arbitrary = LText.pack <$> arbitrary

instance Arbitrary Alias where
  arbitrary = computeAlias <$> arbitrary
  shrink = genericShrink

emptyHost = fromJust $ mkHost ""

composePathPieces [] _ = Nothing
composePathPieces (x : xs) trailingSlash = Just (trailingSlash, x :| xs)

instance Arbitrary ValidURI where
  arbitrary = do
    scheme <- oneof $ map (return . mkScheme) ["http", "https"]
    authority <- arbitrary `suchThat` ((/= emptyHost) . authHost)
    query <- arbitrary
    fragment <- arbitrary
    pathPieces <- arbitrary
    trailingSlash <- arbitrary
    let uri = URI scheme (Right authority) (composePathPieces pathPieces trailingSlash) query fragment
    return $ fromJust $ validateURI uri
  shrink = genericShrink
