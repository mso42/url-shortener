FROM debian:stable AS build

  ENV DEBIAN_FRONTEND noninteractive
  RUN apt-get update
  RUN apt-get -y upgrade
  RUN apt-get -y install curl
  RUN curl -sSL https://get.haskellstack.org/ | sh

  WORKDIR /opt/build

  COPY package.yaml ./
  COPY stack.yaml ./
  COPY stack.yaml.lock ./
  RUN stack setup

  COPY . ./
  RUN stack build
  RUN stack install

FROM debian:stable as runtime

  ENV DEBIAN_FRONTEND noninteractive
  RUN apt-get update
  RUN apt-get -y upgrade

  COPY --from=build /root/.local/bin/ /usr/local/bin/

  USER daemon

  ENV PORT=3000
  EXPOSE 3000

  CMD /usr/local/bin/feeld-challenge-exe
