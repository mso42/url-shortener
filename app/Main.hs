{-# LANGUAGE OverloadedStrings #-}

module Main where

import Data.Maybe (fromMaybe)
import qualified Data.Text as Text
import Lib.Handler (mainHandler, makeIOHandler)
import Lib.UrlStore (emptyInMemoryUrlStore)
import Lib.Web (makeApp)
import qualified Network.Wai as Wai
import Network.Wai.Handler.Warp (defaultSettings, runSettings, setLogger, setPort)
import Network.Wai.Logger (withStdoutLogger)
import System.Environment (lookupEnv)

main :: IO ()
main = do
  port <- getEnvDefaultRead "PORT" 3000
  serverPrefix <- Text.pack <$> getEnvDefault "SERVER_PREFIX" ""
  application <- app serverPrefix
  withStdoutLogger $ \logger -> do
    let settings = setLogger logger $ setPort port $ defaultSettings
    runSettings settings application

app :: Text.Text -> IO Wai.Application
app serverPrefix = do
  store <- emptyInMemoryUrlStore
  let ioHandler = makeIOHandler store serverPrefix mainHandler
  makeApp ioHandler

getEnvDefaultRead :: Read a => String -> a -> IO a
getEnvDefaultRead name def = do
  maybeString <- lookupEnv name
  let maybeVal = read <$> maybeString
  return $ fromMaybe def maybeVal

getEnvDefault :: String -> String -> IO String
getEnvDefault name def = do
  maybeString <- lookupEnv name
  return $ fromMaybe def maybeString
