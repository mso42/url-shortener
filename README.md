# URL shortener (Feeld coding challenge)

## Build and run the tests

Using `stack build` and `stack test`

## Running

A Dockerfile is provided, which exposes port 3000 by default. You should
specify a `SERVER_PREFIX` environment variable, so that returned URLs have the
correct prefix. Something like `SERVER_PREFIX=http://localhost:3000` if running
locally.

There is no TLS support -- use a reverse proxy such as nginx for TLS termination.

## Usage

Submit a POST request to `/shorten` with the target URL in the body; go to the
returned alias path to be redirected to the target URL.

```
% curl -XPOST http://localhost:3000/shorten -d 'https://example.com/a/really/long/url/oh/no'
http://localhost:3000/4DiJdRyg

% curl -i http://localhost:3000/4DiJdRyg
HTTP/1.1 302 Found
Transfer-Encoding: chunked
Date: Mon, 21 Jun 2021 09:22:02 GMT
Server: Warp/3.3.16
Location: https://example.com/a/really/long/url/oh/no
```
