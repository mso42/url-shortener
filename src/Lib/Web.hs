{-# LANGUAGE OverloadedStrings #-}

module Lib.Web
  ( makeApp,
  )
where

import qualified Data.ByteString.Lazy as LBS
import qualified Data.Text.Lazy as LText
import Lib.Handler
  ( IOHandler,
    MyErrorResponse (NotFound, ValidationError),
    MyOkResponse (Created, Redirect),
    MyRequest (Follow, Shorten),
  )
import Network.HTTP.Types.Status as Status
import Network.Wai as Wai
import qualified Web.Scotty as Sc

makeApp :: IOHandler -> IO Wai.Application
makeApp ioHandler = do
  Sc.scottyApp $ routes ioHandler

routes :: IOHandler -> Sc.ScottyM ()
routes ioHandler = do
  Sc.get "/:alias" $ do
    alias <- Sc.param "alias"
    let myRequest = Follow $ LText.toStrict alias
    response <- Sc.liftAndCatchIO $ ioHandler myRequest
    transformResponse response

  Sc.post "/shorten" $ do
    body <- Sc.body
    let myRequest = Shorten $ LBS.toStrict body
    response <- Sc.liftAndCatchIO $ ioHandler myRequest
    transformResponse response

transformResponse :: Either MyErrorResponse MyOkResponse -> Sc.ActionM ()
transformResponse (Right (Created aliasText)) = do
  Sc.status Status.status201
  Sc.text $ LText.fromStrict aliasText
transformResponse (Right (Redirect urlText)) = do
  Sc.redirect $ LText.fromStrict urlText
transformResponse (Left NotFound) = do
  Sc.status Status.status404
transformResponse (Left (ValidationError text)) = do
  Sc.status Status.status400
  Sc.text $ LText.fromStrict text
