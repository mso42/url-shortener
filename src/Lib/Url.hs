{-# LANGUAGE DataKinds #-}
{-# LANGUAGE DeriveGeneric #-}
{-# LANGUAGE OverloadedStrings #-}

module Lib.Url
  ( ValidURI,
    fromUtf8,
    render,
    renderBs,
    validateURI,
  )
where

import Data.ByteString as BS
import Data.Maybe (fromJust)
import Data.Text (Text)
import Data.Text.Encoding (decodeUtf8')
import GHC.Generics (Generic)
import Text.URI
  ( RText,
    RTextLabel (Host, Scheme),
    URI,
    authHost,
    mkHost,
    mkScheme,
    mkURI,
    uriAuthority,
    uriScheme,
  )
import qualified Text.URI as URI

newtype ValidURI = ValidURI URI
  deriving (Show, Eq, Generic)

httpScheme :: RText 'Scheme
httpScheme = fromJust $ mkScheme "http"

httpsScheme :: RText 'Scheme
httpsScheme = fromJust $ mkScheme "https"

emptyHost :: RText 'Host
emptyHost = fromJust $ mkHost ""

fromUtf8 :: BS.ByteString -> Maybe ValidURI
fromUtf8 bs = do
  asText <- toMaybe $ decodeUtf8' bs
  uri <- mkURI asText
  validateURI uri

toMaybe :: Either a b -> Maybe b
toMaybe (Left _) = Nothing
toMaybe (Right r) = Just r

render :: ValidURI -> Text
render (ValidURI uri) = URI.render uri

renderBs :: ValidURI -> BS.ByteString
renderBs (ValidURI uri) = URI.renderBs uri

validateURI :: URI -> Maybe ValidURI
validateURI uri = do
  let isHTTP = uriScheme uri == Just httpScheme
      isHTTPS = uriScheme uri == Just httpsScheme
      host = (/= emptyHost) . authHost <$> uriAuthority uri
      hasValidHost = host == Right True
      isValid = (isHTTP || isHTTPS) && hasValidHost
  if isValid
    then Just $ ValidURI uri
    else Nothing
