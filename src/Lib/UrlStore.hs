module Lib.UrlStore
  ( InMemoryUrlStore,
    UrlStore,
    emptyInMemoryUrlStore,
    get,
    put,
  )
where

import qualified Control.Concurrent.STM as STM
import qualified Control.Concurrent.STM.TVar as TVar
import qualified Data.Map as M
import Lib.Alias (Alias)
import Lib.Url (ValidURI)

class UrlStore u where
  get :: u -> Alias -> IO (Maybe ValidURI)
  put :: u -> Alias -> ValidURI -> IO ()

newtype InMemoryUrlStore = InMemoryUrlStore {storeTVar :: TVar.TVar (M.Map Alias ValidURI)}

instance UrlStore InMemoryUrlStore where
  get store alias = do
    map' <- TVar.readTVarIO $ storeTVar store
    return $ M.lookup alias map'
  put store alias url = do
    let modificationAction = TVar.modifyTVar (storeTVar store) (M.insert alias url)
    STM.atomically modificationAction

emptyInMemoryUrlStore :: IO InMemoryUrlStore
emptyInMemoryUrlStore = InMemoryUrlStore <$> TVar.newTVarIO M.empty
