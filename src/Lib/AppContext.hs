module Lib.AppContext
  ( AppContext (..),
    WithAppContext,
    getAppContext,
    getAppContext',
    runWithAppContext,
  )
where

import qualified Control.Monad.Except as E
import qualified Control.Monad.Reader as R
import qualified Data.Text as Text

data AppContext s = AppContext {getStore :: s, getServerPrefix :: Text.Text}

type WithAppContext store err = R.ReaderT (AppContext store) (E.ExceptT err IO)

runWithAppContext :: AppContext store -> WithAppContext store err a -> IO (Either err a)
runWithAppContext ctx action = E.runExceptT $ R.runReaderT action ctx

getAppContext :: WithAppContext store err (AppContext store)
getAppContext = R.ask

getAppContext' :: (AppContext store -> a) -> WithAppContext store err a
getAppContext' = R.asks
