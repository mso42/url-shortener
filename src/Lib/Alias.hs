{-# LANGUAGE DeriveGeneric #-}

module Lib.Alias
  ( Alias,
    computeAlias,
    fromText,
    getAliasText,
  )
where

import Crypto.Hash (Digest, hash)
import Crypto.Hash.Algorithms (Blake2b_160)
import Data.ByteArray (convert)
import qualified Data.ByteString as BS
import Data.ByteString.Base64.URL (encodeBase64)
import qualified Data.Text as Text
import GHC.Generics (Generic)
import Lib.Url (ValidURI, renderBs)

newtype Alias = Alias {getAliasText :: Text.Text}
  deriving (Show, Eq, Ord, Generic)

computeAlias :: ValidURI -> Alias
computeAlias uri =
  let digest :: Digest Blake2b_160
      digest = hash $ renderBs uri
      digestAsBs :: BS.ByteString
      digestAsBs = convert digest
      base64Encoded :: Text.Text
      base64Encoded = encodeBase64 digestAsBs
   in Alias $ Text.take 8 base64Encoded

webSafeCharacters :: [Char]
webSafeCharacters = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789-_="

fromText :: Text.Text -> Maybe Alias
fromText text
  | Text.length text /= 8 = Nothing
  | not $ Text.all (`elem` webSafeCharacters) text = Nothing
  | otherwise = Just $ Alias text
