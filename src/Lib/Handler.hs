{-# LANGUAGE OverloadedStrings #-}

module Lib.Handler
  ( Handler,
    IOHandler,
    MyErrorResponse (..),
    MyOkResponse (..),
    MyRequest (..),
    mainHandler,
    makeIOHandler,
  )
where

import Control.Monad.Except (liftEither)
import Control.Monad.Trans (liftIO)
import qualified Data.ByteString as BS
import qualified Data.ByteString.Lazy as LBS
import qualified Data.Text as Text
import qualified Data.Text.Lazy as LText
import Lib.Alias (computeAlias, fromText, getAliasText)
import Lib.AppContext
  ( AppContext (AppContext),
    WithAppContext,
    getAppContext',
    getServerPrefix,
    getStore,
    runWithAppContext,
  )
import Lib.Url (fromUtf8, render)
import Lib.UrlStore (UrlStore, get, put)

data MyRequest
  = Shorten {body :: BS.ByteString}
  | Follow {aliasText :: Text.Text}
  deriving (Show, Eq)

data MyOkResponse = Created Text.Text | Redirect Text.Text
  deriving (Show, Eq)

data MyErrorResponse = ValidationError Text.Text | NotFound
  deriving (Show, Eq)

type Handler s = MyRequest -> WithAppContext s MyErrorResponse MyOkResponse

type IOHandler = MyRequest -> IO (Either MyErrorResponse MyOkResponse)

makeIOHandler :: store -> Text.Text -> Handler store -> IOHandler
makeIOHandler store serverPrefix handler request = runWithAppContext context $ handler request
  where
    context = AppContext store serverPrefix

invalidUri :: MyErrorResponse
invalidUri = ValidationError "Not a valid URI"

invalidAlias :: MyErrorResponse
invalidAlias = ValidationError "Not a valid alias"

mainHandler :: UrlStore s => Handler s
mainHandler (Shorten body') = do
  url <- liftEither $ maybeToEither invalidUri $ fromUtf8 body'
  let alias = computeAlias url
  store <- getAppContext' getStore
  () <- liftIO $ put store alias url
  serverPrefix <- getAppContext' getServerPrefix
  let prefixedAliasText = Text.concat [serverPrefix, "/", getAliasText alias]
  return $ Created prefixedAliasText
mainHandler (Follow aliasText') = do
  alias <- liftEither $ maybeToEither invalidAlias $ fromText aliasText'
  store <- getAppContext' getStore
  maybeUri <- liftIO $ get store alias
  uri <- liftEither $ maybeToEither NotFound maybeUri
  return $ Redirect $ render uri

maybeToEither :: a -> Maybe b -> Either a b
maybeToEither l Nothing = Left l
maybeToEither _ (Just v) = Right v
